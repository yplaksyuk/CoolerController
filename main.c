/* Name: main.c
 * Author: Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

volatile uint8_t cycle = 0;
volatile uint8_t value = 0;

int main(void)
{
	// Configure system clock: F = 9.6MHz / 32 = 300kHz
	CLKPR = (1 << CLKPCE);                                    // Enable prescaler change
	CLKPR = (1 << CLKPS2) | (1 << CLKPS0);                    // Write new prescaler value - 256 (F = 300kHz)

	// Configure timer: f(timer) = F / 1024 = 293Hz
	// Timer/Clock overflows will occur at f(timer) / 256 = 1.14Hz
	TCCR0A = 0;                                               // Normal operation
	TCCR0B = (1 << CS02) | (1 << CS00);                       // Internal clock source/256 prescaling
	TIMSK0 = (1 << TOIE0);                                    // Enable timer interrupts on overflow

	// Configure ADC: f(adc) = F / 4 = 75kHz
	ADMUX = (1 << ADLAR) | (1 << MUX1) | (1 << MUX0);         // Select ADC3 (PB3) as ADC input
	ADCSRA = (1 << ADEN) | (1 << ADATE) | (1 << ADIE) | (1 << ADPS1);
	ADCSRB = (1 << ADTS2);                                    // Trigger Source: Timer Overflow
	DIDR0 = (1 << ADC3D);                                     // Disable logic input on ADC3 pin
	
	// Configure pins
	DDRB = (1 << DDB4);
	PORTB = 0;

	set_sleep_mode(SLEEP_MODE_IDLE);                          // Set sleep mode - IDLE
	sleep_enable();
	sei();

    while (1) {
		sleep_cpu();
    }

    return 0;
}

ISR(TIM0_OVF_vect, ISR_NAKED)
{
	if (cycle == 0) {                                         // T = 0.0s
		PORTB |= (1 << PORTB4);                               // Turn cooler ON
	}
	else if (cycle == 2) {                                    // T = 1.4s
		if (value < 140)                                      // if value is OK ...
			PORTB &= ~(1 << PORTB4);                          // ... turn cooler OFF
		else                                                  // else
			cycle = 0;                                        // ... reset cycle (loop)
	}

	cycle++;
	reti();
}

ISR(ADC_vect, ISR_NAKED)
{
	value = ADCH;
	reti();
}
